Os nomes de bandas e músicas devem ser convertidos para _varacase_ (apenas
letras e números em caixa baixa, sem espaços ou caracteres especiais),
exemplos:

* `Machete Bomb` vira `machatebomb`
* `AC/DC` vira `acdc`

Cada banda deve conter um arquivo, exemplo:

* machetebomb.md
* acdc.md

Músicas devem ser armazenadas em diretórios com o nome da banda, exemplos:

* acdc/highwaytohell.md

Regras:

* Não pode k pop.
